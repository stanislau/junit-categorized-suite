package org.flippy.junit;

import org.flippy.junit.usage.CategorizedSuiteDefaultPackage;
import org.flippy.junit.usage.CategorizedSuiteWithSpecifiedPackage;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(value = Suite.class)
@Suite.SuiteClasses(value = {
        CategorizedSuiteDefaultPackage.class,
        CategorizedSuiteWithSpecifiedPackage.class, 
        
        CategorizedSuiteTest.class
})
public class AllTests {

}
