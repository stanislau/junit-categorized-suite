package org.flippy.junit.usage;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.junit.Assert.assertTrue;

@Category(value = {IntegrationTest.class, SlowTest.class})
public class CategorizedAsSlowAndIntegrationTest {
    @Test
    public void test() {
        assertTrue(true);
    }
}
