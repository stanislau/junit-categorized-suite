package org.flippy.junit.usage;

import org.flippy.junit.BasePackage;
import org.flippy.junit.Categories;
import org.flippy.junit.CategorizedSuite;
import org.junit.runner.RunWith;

@Categories(categoryClasses = {IntegrationTest.class, SlowTest.class})
@BasePackage(name = "org.flippy")
@RunWith(CategorizedSuite.class)
public class CategorizedSuiteWithSpecifiedPackage {

}
