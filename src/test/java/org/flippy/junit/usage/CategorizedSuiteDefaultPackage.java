package org.flippy.junit.usage;

import org.flippy.junit.Categories;
import org.flippy.junit.CategorizedSuite;
import org.flippy.junit.Categories.CombinationRule;
import org.junit.runner.RunWith;

@Categories(categoryClasses = {IntegrationTest.class, AnotherTest.class}, rule = CombinationRule.AND)
@RunWith(CategorizedSuite.class)
public class CategorizedSuiteDefaultPackage {

}
