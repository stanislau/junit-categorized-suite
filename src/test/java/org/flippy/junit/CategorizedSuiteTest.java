package org.flippy.junit;

import static org.junit.Assert.*;

import org.flippy.junit.usage.CategorizedSuiteWithSpecifiedPackage;
import org.junit.Test;

public class CategorizedSuiteTest {
    @Test
    public void testComputeSuiteClasses() throws Exception {
        Class<?> [] classes = CategorizedSuite.computeSuiteClasses(CategorizedSuiteWithSpecifiedPackage.class); 
        assertEquals("Number of suite classes should be equal to 3", 3, classes.length);
    }
}
