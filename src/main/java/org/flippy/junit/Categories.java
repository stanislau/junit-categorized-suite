package org.flippy.junit;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import static org.flippy.junit.Categories.CombinationRule.OR;

/**
 * Annotation for enumerating category classes for a suite 
 * 
 * @author Stanislau
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
public @interface Categories {
    /**
     * @return categories of classes to be run
     */
    public Class<?>[] categoryClasses();

    /**
     * @return rule to combine Category classes
     */
    public CombinationRule rule() default OR;

    public enum CombinationRule {
        AND, OR
    }
}